import React from 'react';
import { 
  Navbar,
  img,
  Container,
  Nav,
  NavDropdown,
  Row,
  Col,
  Form,
  h5,
  br,
  h6

} from 'react-bootstrap';


function Header(){
  return(
   <Navbar expand="lg">
  <Navbar.Brand href="#home">  
  <img
        src="https://cimory.com/wp-content/uploads/2016/10/cropped-logo-cimory-new.png"
        width="140"
        height="90"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
      </Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
     
    </Nav>
    <Form inline>
       <NavDropdown title="Our Brands" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
       <Nav.Link href="#home">About Us</Nav.Link>
       <Nav.Link href="#home">Our Events</Nav.Link>
       <Nav.Link href="#home">Articles</Nav.Link>
       <Nav.Link href="#home">Stores Location</Nav.Link>
       <Nav.Link href="#home">Career</Nav.Link>
       <Nav.Link href="#home">Contact Us</Nav.Link>
    </Form>
  </Navbar.Collapse>
</Navbar>
    );
}




function Menu() {
  return(
      <div>
        <Row>
          <Col xs={6} md={3} style={{textAlign:"center"}}>
            <img src="https://cimory.com/wp-content/uploads/2019/05/Blueberry-Small.png" height="294" thumbnail />
            <br/><br/><br/><h5>Yogurt Drink 70ml</h5>
          </Col>
          <Col xs={6} md={3} style={{textAlign:"center"}}>
            <img src="https://cimory.com/wp-content/uploads/2016/10/cimory-kotak-mix-berry.png" height="294" thumbnail />
            <br/><br/><br/><h5>Yogurt Drink 200ml</h5>
          </Col>
          <Col xs={6} md={3} style={{textAlign:"center"}}>
            <img src="https://cimory.com/wp-content/uploads/2019/05/raspberry.png" thumbnail />
            <br/><br/><br/><h5>Yogurt Drink 250ml</h5>
          </Col>
          <Col xs={6} md={3} style={{textAlign:"center"}}>
            <img src="https://cimory.com/wp-content/uploads/2019/05/Yogurt-Drink-Yolite-Lemon.png" thumbnail />
            <br/><br/><br/><h5>Yolite C+100 250ml</h5>
          </Col>
        </Row>
      </div>
  )
}

function Footer(){
  return(
   <div>
      <h6 style={{textAlign:"center",marginTop:"50px"}}>
      Copyright 2018. Cimory Group all rights reserved.
      </h6><br/><br/>
</div>
);
}




 function App() {
  return(
    <Container>
      <Header/>
      <Menu/>
      <Footer/>
    </Container>
    );
 }



 export default App;